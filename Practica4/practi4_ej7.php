<!-- // Ejercicio 7  -->

<?php

$dia = 6;


// realizar un codigo que me permite indicar a que día de la semana pertenece:
// Lunes
// Martes


// Usando IF

if ($dia == 1) {
    $salida = "Lunes";
} elseif ($dia == 2) {
    $salida = "Martes";
} elseif ($dia == 3) {
    $salida = "Miercoles";
} elseif ($dia == 4) {
    $salida = "Jueves";
} elseif ($dia == 5) {
    $salida = "Viernes";
} elseif ($dia == 6) {
    $salida = "Sabado";
} elseif ($dia == 7) {
    $salida = "Domingo";
} else {
    $salida = "Error";
}

echo $salida;

// Usando switch

switch ($dia) {
    case '1':
        $salida = "Lunes";
        break;
    case '2':
        $salida = "Martes";
        break;
    case '3':
        $salida = "Miercoles";
        break;
    case '4':
        $salida = "Jueves";
        break;
    case '5':
        $salida = "Viernes";
        break;
    case '6':
        $salida = "Sabado";
        break;
    case '7':
        $salida = "Domingo";
        break;

    default:
        $salida = "Error";
        break;
}

echo $salida;

//Usando un array

$diasSemana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado", "Domingo"];

// Comprobamos que el indice exista en el array
// Si existe cogemos el valor del array
if (array_key_exists($dia, $diasSemana)) {
    // Como el array empieza en 0 y el primero registro (Lunes) corresponde al dia 1, le restamos 1
    $salida = $diasSemana[$dia - 1];
} else {
    // Si no existe el indice $salida vale "Error"
    $salida = "Error";
}

echo $salida;



?>