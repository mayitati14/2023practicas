<!-- indicar la salida que se produce con el siguiente codigo php. -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <?php
    $alumno1 = "Ramon";
    $alumno2 = "Alberto";
    $alumno3 = "Marcos";
    $alumno4 = "Maria";

    $a = [23, 44, 55, 67, 89];

    foreach ($a as $v) {
        echo "<br>$v";
    }

    ?>

    <div>
        <?php
        echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
        ?>
    </div>
</body>

</html>