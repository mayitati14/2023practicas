<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $entero = 10;
    $cadena = "hola";
    $real = 23.6;
    $logico = TRUE;

    var_dump($entero);
    var_dump($cadena);
    var_dump($real);
    var_dump($logico);

    $logico = (int)$logico;
    $entero = (float)$entero;
    settype($logico, "int");

    var_dump($entero);
    var_dump($cadena);
    var_dump($real);
    var_dump($logico);


    ?>

</body>

</html>