<!-- vamos a trabajar con arrays  -->

<?php

// crear arrays vacios

$numeros = [];
$textos = [];

// inspeccionar el contenido de los arrays 

var_dump($numeros, $textos);

// // crear un array con las vocales (introducir los valores a mano).
// mostrar el contenido de la variable utilizando la funcion var_dump
?>




// opcion 1
<?php
$vocales = [
    "a", "e", "i", "o", "u"
];

var_dump($vocales);


?>
<!-- 
// vamos a crear un array asociativo  -->

<?php

$numeros = [
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,

];

var_dump($numeros);

?>
<!-- 
ejercicio 4 -->

<?php

$numeros = [
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,

];
$vocales = [
    "a", "e", "i", "o", "u"
];

// leer el segundo elemento del array numeros y mostrarlo en pantalla (1)
echo $numeros["uno"];

// leer el segundo elemento del array vocales y mostrarlo en pantalla (e)
echo $vocales[1];
// añadir un elemento al final de vocales con la a con tilde  

// opcion 1
$vocales[] = "á";
//opcion 2
array_push($vocales, "á");

var_dump($vocales);

// añadir un elemento nuevo al array numercos con indice cinco con el valor 5  
$numeros["cinco"] = 5;
var_dump($numeros);



?>

<?php
$datos = [
    [
        "nombre" => "Eva",
        "edad" => 50
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80
    ]
];

// mostrar el nombre del primer dato


// mostrar el nombre del segundo dato

// mostrar el peso 


?>
<!-- 
// introducir a "Lorena" de 80 años y con una altura de 175

// realizarlo directamente

//introducir a "Luis" de 20 años y con un peso de 90 y a "Oscar" de 23 años
// realizarlo mediante la funcion push -->