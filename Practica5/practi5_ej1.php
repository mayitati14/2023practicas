<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="">
        <div>
            <label for="numero1">Número 1</label>
            <input type="number" name="numero1" id="numero1">
        </div>
        <div>
            <label for="numero2">Número 2</label>
            <input type="number" name="numero2" id="numero2">
        </div>
        <div>
            <button name="suma">Suma</button>
            <button name="resta">Resta</button>
            <button name="producto">Producto</button>
            <button name="division">División</button>
            <button name="raizCuadrada"> Raiz Cuadrada</button>
            <button name="potencia">Potencia</button>
        </div>
    </form>

    <?php
    $numero1 = 0;
    $numero2 = 0;
    $resultado = 0;
    if (isset($_GET["suma"])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "La suma es: " .  $numero1 + $numero2;
        echo $resultado;
    } elseif (isset($_GET['resta'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "La resta es: " . $numero1 - $numero2;
        echo $resultado;
    } elseif (isset($_GET['producto'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "El producto es: " . $numero1 * $numero2;
        echo $resultado;
    } elseif (isset($_GET['division'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "La división es: " . $numero1 / $numero2;
        echo $resultado;
    } elseif (isset($_GET['raizCuadrada'])) {
        $numero1 = $_GET['numero1'];

        $resultado = "La raiz cuadrada del número 1: " . $numero1 ** (1 / 2);
        echo $resultado;
    } else if (isset($_GET['potencia'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "El primer número elevado al segundo es: " . $numero1 ** $numero2;
        echo $resultado;
    }



    ?>
</body>

</html>