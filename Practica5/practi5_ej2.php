<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="">
        <div>
            <label for="numero1">Número 1</label>
            <input type="number" name="numero1" id="numero1">
        </div>
        <div>
            <label for="numero2">Número 2</label>
            <input type="number" name="numero2" id="numero2">
        </div>
        <div>
            <button name="calcular">Calcular</button>

        </div>
    </form>

    <?php
    $numero1 = 0;
    $numero2 = 0;
    $suma = 0;
    $resta = 0;
    $producto = 0;
    $division = 0;
    if (isset($_GET["calcular"])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];
        $suma = $numero1 + $numero2;
        $resta = $numero1 - $numero2;
        $producto = $numero1 * $numero2;
        $division = $numero1 / $numero2;

        echo "<div>";

        echo "<h2> Número 1: {$numero1}</h2>";
        echo "<h2> Número 2: {$numero2}</h2>";

        echo "<ul>";
        echo "<li style= 'list-style-type: square'> Suma: {$suma}</li>";
        echo "<li style= 'list-style-type: square'> Resta: {$resta}</li>";
        echo "<li style= 'list-style-type: square'> Producto: {$producto}</li>";
        echo "<li style= 'list-style-type: square'> División: {$division}</li>";
        echo "</ul>";
        echo "</div>";
    }




    ?>
</body>

</html>