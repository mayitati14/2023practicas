<!-- Ejericio 4 
Hacer que cada vez que cargue una página web debe colocar un numero entero entre 1 y 10.
Para ello debéis utilizar la función mt_rand.
Si el numero es par debe indicarlo con el mensaje "el número es par" -->


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    $numero = mt_rand(1, 10);

    echo $numero . "<br>";

    if ($numero % 2 == 0) {
        echo "El número es par";
    } else {
        echo "El número es impar";
    }

    ?>
</body>

</html>